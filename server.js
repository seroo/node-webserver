var express = require("express");
var app = express();
var PORT = process.env.PORT || 3000;//herokuda calisacagi icin  bu olmuo 3000;
var middleware = require("./middleware");



//bu metod cagrisi asagidakilerin ustunde ayri bi sekilde cagirilinca hepsine isler
//app.use(middleware.requireAuthentication);
app.use(middleware.logger);

/*
app.get("/", function(req, res){
    res.send("Express serverdannnn Merhaba!!")
});*/
                    //buraya eklenince sadece hakkımda sayfasında login olma durumu kontrol edilir gibi dusun
app.get("/hakkinda", middleware.requireAuthentication, function(req, res){
    res.send("Hakkımda sayfası")
})

//bu metod kullanılırsa yukarıdaki app.get("/" da gerek yok 
app.use(express.static(__dirname + "/public"));

//bulunduğun klasörü gsöterir
//console.log(__dirname);



app.listen(PORT, function(){
    console.log("listening port" + PORT);
    
});